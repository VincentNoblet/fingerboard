#!/usr/bin/env bash
set -e

npm run dev

docker build --build-arg TARGET_VERSION=$TARGET_VERSION -t xivoxc/fingerboard .

