angular.module('apps.controller', [])
.controller('AppsController', function($scope, $window, $timeout, AppsServices) {

    const imagePath = "../assets/images/"; 
    const cssPath = "../assets/css/"  
    
   
    $scope.appList = AppsServices.getAppList();

    $scope.getActiveImage = function(app) {              
      return imagePath + app.name + ".svg" ;
    }
    $scope.getHoverImage = function(app) {          
      return imagePath + app.name + "-hover.svg" ;
    }
    $scope.getHeadBandName = function(app) {          
      return app.headBandName;
    }
    $scope.open = function(app) {  
      if (app.on) {
        $window.open(app.url,"_self");
      }
    }
    $scope.popoverColor = function(app) {   
      if (app.on) {
        return "popover-green" ;
      }  
      else {
        return "popover-red" ;
      }
    }
    $scope.getLightImage = function(app) {  
      if (app.on){
        return imagePath + "green-light.svg"
      }
      else{
        return imagePath + "red-light.svg"            
      }
    }
    $scope.getStatus = function(app) {  
      return(app.response.status);
    }
    $scope.getHeader = function(app) {  
      return(app.response.header);
    }
    var update = function(){    
      AppsServices.updateAllOn();

      $timeout(update, 10000);
    }
    update();
})